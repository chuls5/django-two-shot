from django.urls import path

from receipts.views import (
    receipt_list,
    create_receipt,
    list_category,
    create_category,
    list_account,
    create_account,
)

urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", list_category, name="list_category"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", list_account , name="list_account"),
    path("accounts/create/", create_account, name="create_account"),
]
