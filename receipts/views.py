from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
# Feature 5 - ReceiptListView + login required
@login_required(login_url="login")
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


# Feature 11 - ReceiptCreateView + login required
@login_required(login_url="login")
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# Feature 12 - CategoryListView & AccountListView
@login_required(login_url="login")
def list_category(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {
        "list_category": categories,
        "receipts": receipts,
    }
    return render(request, "receipts/categories.html", context)


#Feature 12- account list view- login required
@login_required(login_url="login")
def list_account(request):
    accounts = Account.objects.filter(owner=request.user)
    receipts = Receipt.objects.all()
    context = {
        "list_account": accounts,
        "receipts": receipts
    }
    return render(request, "receipts/accounts.html", context)


# Feature 13 - Create Category View
@login_required(login_url="login")
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("list_category")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)

# Feature 14 - Create Account View
@login_required(login_url="login")
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("list_account")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
