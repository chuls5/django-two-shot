from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

# Feature 11
class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

# Feature 13
class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name"
        ]

# Feature 14
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number"
        ]
